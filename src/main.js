
import axios from 'axios';
import VueAxios from 'vue-axios';


import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

createApp(App).use(store).use(router).use(VueAxios,axios).mount('#app');//特殊寫法解決axios載入問題

axios.defaults.withCredentials = true;//設定cross跨域 並設定訪問許可權 允許跨域攜帶cookie資訊





